import * as flsFunctions from "./modules/functions.js";

flsFunctions.isWebp();


const getProductBlockAndMainBlock = (event) => {
    document.querySelector('.btn_active')?.classList.remove('btn_active');
    event.currentTarget.classList.toggle('btn_active');
    let productBlock = document.querySelectorAll('.sport-goods__wrapper');
    let mainBlock = document.querySelector('.sport-goods-container');
    return {productBlock, mainBlock};
}

const onClickSortButtonPrice = (event) => {
    let {productBlock, mainBlock} = getProductBlockAndMainBlock(event);
    let arrayOfProductPrices = Array.prototype.slice.call(productBlock);
    arrayOfProductPrices.sort(function (a, b) {
        return parseInt(a.querySelector('.price__number').innerText.replace(/[^\d]/g, '')) - parseInt(b.querySelector('.price__number').innerText.replace(/[^\d]/g, ''))
    })
    arrayOfProductPrices.forEach((sortElement => {
        mainBlock.appendChild(sortElement);
    }))
}

const onClickSortButtonPopularity = (event) => {
    let {productBlock, mainBlock} = getProductBlockAndMainBlock(event);
    let arrayOfProduct = Array.prototype.slice.call(productBlock);
    arrayOfProduct.reverse()
    arrayOfProduct.forEach((sortElement => {
        mainBlock.appendChild(sortElement);
    }))
}

const onClickSortButtonAlphabet = (event) => {
    let {productBlock, mainBlock} = getProductBlockAndMainBlock(event);
    let arrayOfProduct = Array.prototype.slice.call(productBlock);
    arrayOfProduct.sort(function (a, b) {
        return a.querySelector('.content__title').innerText.toLowerCase().localeCompare(b.querySelector('.content__title').innerText.toLowerCase())
    })
    arrayOfProduct.forEach((sortElement => {
        mainBlock.appendChild(sortElement);
    }))
}


const setOnClickSortButtons = () => {
    let btnSortByPrice = document.querySelector('.filter-brands__price');
    let btnSortByPopularity = document.querySelector('.filter-brands__popularity');
    let btnSortByAlphabet = document.querySelector('.filter-brands__alphabet');
    btnSortByPrice.addEventListener('click', onClickSortButtonPrice, false);
    btnSortByPopularity.addEventListener('click', onClickSortButtonPopularity, false);
    btnSortByAlphabet.addEventListener('click', onClickSortButtonAlphabet, false);
}

setOnClickSortButtons();


const OnClickImageToChange = (event) => {
    let maxImage = event.target.parentElement.parentNode?.previousElementSibling.querySelector('.sport-goods__image-max');
    maxImage.src = event.target.getAttribute('src');
}

const setOnClickImageToChange = (image) => {
    image.addEventListener('click', OnClickImageToChange, false);
}

document.querySelectorAll(".sport-goods__image").forEach(image => setOnClickImageToChange(image));


const onclickButtonShowMore = (event) => {
    let textForShowMore = event.target.previousElementSibling.querySelector('.text-more__close');
    textForShowMore.classList.toggle('text-more__open');
}

const setOnClickBtnShowMore = (button) => {
    button.addEventListener('click', onclickButtonShowMore, false);
}

document.querySelectorAll(".text-more__show").forEach(button => setOnClickBtnShowMore(button));


const onClickMenuBtn = (event) => {
    let wrapper = document.querySelector('.header__wrapper')
    let sidebar = document.querySelector('.navigation__body-product');
    sidebar.classList.toggle('sidebar_open');
    if (sidebar.classList.contains('sidebar_open')) {
        let width = window.innerWidth <= 450 ? `${window.innerWidth}px` : `${window.innerWidth}px`;
        sidebar.style.height = `auto`;
        sidebar.style.width = width;
        wrapper.style.position = 'relative'
    } else {
        sidebar.style.width = '0px';
        document.body.style.overflow = "";
        wrapper.style.position = 'sticky'

    }
}
const onClickMenuBtnToggle = (event) => {
    let dropdownMenu = event.currentTarget.querySelector('.navigation__wrapper-sublist');
    dropdownMenu.classList.toggle('menu-active');
}

const setOnClickMenuBtnToggle = (button) => {
    button.addEventListener('click', onClickMenuBtnToggle, false);
}

document.querySelectorAll(".navigation__border").forEach(button => setOnClickMenuBtnToggle(button));


const setOnClickMenuBtn = () => {
    let btnSortByPrice = document.querySelector('.menu__btn');
    btnSortByPrice.addEventListener('click', onClickMenuBtn, false)
}

setOnClickMenuBtn();


let modalContacts = document.getElementById("modal-contact");
document.querySelectorAll(".btn-modal-get-contacts").forEach((item) => {
    item.onclick = getClickContact;
});
let modalOrder = document.getElementById("modal-order");
document.querySelectorAll(".btn-modal-get-order").forEach((item) => {
    item.onclick = getClickOrder;
});

let closeModalWindowContacts = document.querySelector(".block-modal__close");
let closeModalWindowOrder = document.querySelector(".block-modal__close-order");

function getClickContact() {
    modalContacts.style.display = "block";
    document.body.style.overflow = 'hidden'
}

function getClickOrder() {
    modalOrder.style.display = "block";
    document.body.style.overflow = 'hidden'
}

closeModalWindowContacts.onclick = function () {
    modalContacts.style.display = "none";
    document.body.style.overflow = 'visible'
}
closeModalWindowOrder.onclick = function () {
    modalOrder.style.display = "none";
    document.body.style.overflow = 'visible'
}
window.onclick = function (event) {
    if (event.target === modalContacts || event.target === modalOrder) {
        modalContacts.style.display = "none";
        modalOrder.style.display = "none";
        document.body.style.overflow = 'visible'
    }
}

window.addEventListener("load", () => {
    function sendData() {
        const XHR = new XMLHttpRequest();
        const FD = new FormData(form);
        XHR.addEventListener("load", (event) => {
            alert(event.target.responseText);
        });
        XHR.addEventListener("error", (event) => {
            alert('Что-то пошло не так.');
        });
        XHR.open("POST", "/");
        XHR.send(FD);
    }

    const form = document.getElementById("form-contacts-data");
    form.addEventListener("submit", (event) => {
        event.preventDefault();

        sendData();
    });
});








